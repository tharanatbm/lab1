/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightticketmanagement.domain;

/**
 *
 * @author boom
 */
    public enum Airport {
    
        BKK("Suvarnabhumi Airport","Bangkok(Bang Phli,Samut Prakan)","BKK","TH"),DMK("Don Mueang International Airport","Bangkok","DMK","TH")
        ,HKT("Phuket International Airport","Phuket","HKT","TH"),CNX("Chaing Mai International Airport","Chiang Mai","CNX","TH")
        ,HDY("Hat Yai International Airport","Songkhla","HDY","TH"),CEI("Mae Fah Luang Chaing Rai International Airport","Chiang Rai","CEI","TH");
        
        private final String airportName;
        private final String cityName;
        private final String cityInitial;
        private final String country;
     
        private Airport(String airportName,String cityName,String cityInitial,String country)
        {
             this.airportName = airportName;
             this.cityName = cityName;
             this.cityInitial = cityInitial;
             this.country = country;
             
        }
        public String getAirportName()
        {
              return airportName;
        }
        public String getCityName()
        {
              return cityName;
        }
        public String getCityInitial()
        {
              return cityInitial;
        }
        public String getCountry()
        {
              return country;
        }
    
    }

